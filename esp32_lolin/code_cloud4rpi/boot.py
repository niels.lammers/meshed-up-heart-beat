# Complete project details at
#   https://randomnerdtutorials.com/micropython-mqtt-esp32-esp8266/
import machine
from ubinascii import hexlify
import network

# wifi connection info: enter your local wifi ssid and password.
ssid = '???'
password = '???'

# central mqtt-server: use https://www.hivemq.com/public-mqtt-broker/
mqtt_server =  'broker.hivemq.com'
mqtt_port = 1883
mqtt_client_id = hexlify(machine.unique_id())
mqtt_topic_pub = b'heart-beat/avg-beat'
mqtt_topic_sub1 = b'heart-beat/max-beat'
mqtt_topic_sub2 = b'heart-beat/min-beat'

last_message = 0
message_interval = 15
counter = 0

print('Setting up Wifi-connection')
station = network.WLAN(network.STA_IF)
station.active(True)
station.connect(ssid, password)

while station.isconnected() == False:
    pass

print(station.ifconfig())
print('Wifi-connection successful')
print('boot finished')
